package com.example.examenc1kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import org.w3c.dom.Text
import java.lang.String

class RectanguloActivity : AppCompatActivity() {
    //Variables necesarias
    private lateinit var lblNombre: TextView
    private lateinit var lblBase: TextView
    private lateinit var txtBase: EditText
    private lateinit var lblAltura: TextView
    private lateinit var txtAltura: EditText
    private lateinit var lblCalculoArea: TextView
    private lateinit var lblCalArea: TextView
    private lateinit var lblCalculoPerimetro: TextView
    private lateinit var lblCalPerimetro: TextView

    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private var rectangulo = Rectangulo(0, 0)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo)
        iniciarComponentes()
        val bundle = intent.extras
        lblNombre.text = bundle!!.getString("nombre")

        btnCalcular.setOnClickListener { Calcular() }
        btnLimpiar.setOnClickListener { Limpiar() }
        btnRegresar.setOnClickListener { Regresar() }
    }

    //Funcón para hacer la relación de los objetos
    private fun iniciarComponentes(){
        lblNombre = findViewById(R.id.lblNombre)
        lblBase = findViewById(R.id.lblBase)
        txtBase = findViewById(R.id.txtBase)
        lblAltura = findViewById(R.id.lblAltura)
        txtAltura = findViewById(R.id.txtAltura)
        lblCalculoArea = findViewById(R.id.lblCalculoArea)
        lblCalArea = findViewById(R.id.lblCalArea)
        lblCalculoPerimetro = findViewById(R.id.lblCalPerimetro)
        lblCalPerimetro = findViewById(R.id.lblCalPerimetro)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
    }

    //Función de los botones
    private fun Calcular(){
        if(txtAltura.text.toString().equals("") || txtBase.text.toString().equals("")){
            Toast.makeText(
                applicationContext,
                "Ingrese Datos", Toast.LENGTH_LONG
            ).show()
        }else{
            rectangulo.base = txtBase.text.toString().toInt()
            rectangulo.altura = txtAltura.text.toString().toInt()
            lblCalArea.text = String.valueOf(rectangulo.calcularCalcularArea())
            lblCalPerimetro.text = String.valueOf(rectangulo.calcularPagPerimetro())
        }
    }

    //Función para limpiar
    private fun Limpiar(){
        txtBase.setText("")
        txtAltura.setText("")
        lblCalArea.text = ""
        lblCalPerimetro.text = ""
    }

    //Función para Regresar
    private fun Regresar(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Rectangulo")
        confirmar.setMessage(" ¿Desea regresar? ")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog, which -> finish() }

        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog, which -> dialog.dismiss() }

        confirmar.show()
    }
}