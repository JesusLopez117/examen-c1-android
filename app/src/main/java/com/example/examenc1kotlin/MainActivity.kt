package com.example.examenc1kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    //Variables necesarias
    private lateinit var lblNombre: TextView
    private lateinit var txtNombre: EditText
    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEntrar.setOnClickListener { Entrar() }
        btnSalir.setOnClickListener { Salir() }
    }

    //Iniciar componentes
    private fun iniciarComponentes(){
        lblNombre = findViewById(R.id.lblNombre)
        txtNombre = findViewById(R.id.txtNombre)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    //Función para Entrar
    private fun Entrar(){
        //Validacion de ingreso de campos
        if (txtNombre.text.toString() == "") {
            Toast.makeText(
                applicationContext,
                "Ingrese un nombre", Toast.LENGTH_LONG
            ).show()
        } else {
            val bundle = Bundle()
            bundle.putString("nombre", txtNombre.text.toString())
            val intent = Intent(this@MainActivity, RectanguloActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            txtNombre.setText("")
        }
    }

    //Función para salir
    private fun Salir(){
        finish()
    }
}