package com.example.examenc1kotlin

class Rectangulo {
    //Variables
    var base:Int = 0
    var altura: Int = 0

    //Constructor
    constructor(base:Int, altura:Int){
        this.base = base
        this.altura = altura
    }

    //Funciones necesarias
    fun calcularCalcularArea():Float{
        var area:Float = 0.0F
        area = (((base * altura) / 2).toFloat())
        return area
    }

    //Funcion calcular perimetro
    fun calcularPagPerimetro():Float{
        var perimetro:Float = 0.0F
        perimetro = ((base * 2) + (altura * 2)).toFloat()
        return perimetro
    }
}